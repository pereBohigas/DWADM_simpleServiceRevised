import { Component, OnInit } from '@angular/core';
import { CounterService } from "../counter.service";

@Component({
  selector: 'app-component-a',
  templateUrl: './component-a.component.html',
  styleUrls: ['./component-a.component.scss']
})
export class ComponentAComponent implements OnInit {

  constructor( private counterService: CounterService ) { }

  ngOnInit() {
  }

  addOne() {
    console.log('Component A | Click on plus');
    this.counterService.plusOne();
  }

  substractOne() {
    console.log('Component A | Click on minus');
    this.counterService.minusOne();
  }

}
