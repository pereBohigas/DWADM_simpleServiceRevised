import { Component, OnInit } from '@angular/core';
import { CounterService } from "../counter.service";

@Component({
  selector: 'app-component-b',
  templateUrl: './component-b.component.html',
  styleUrls: ['./component-b.component.scss']
})
export class ComponentBComponent implements OnInit {

  constructor( private counterService: CounterService ) { }

  ngOnInit() {
  }

  addOne() {
    console.log('Component B | Click on plus');
    this.counterService.plusOne();
  }

  substractOne() {
    console.log('Component B | Click on minus');
    this.counterService.minusOne();
  }

}
